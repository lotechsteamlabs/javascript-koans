describe('About Strings and Numbers', () => {
    it('should create strings', () => {
        const emptyString = '';
        expect(typeof(emptyString)).toEqual(FILL_ME_IN);

        const simpleString = 'i love u';
        expect(simpleString[0]).toBe(FILL_ME_IN);
        expect(simpleString[1]).toBe(FILL_ME_IN);
        expect(simpleString[2]).toBe(FILL_ME_IN);
    });

    it('should understand string size (length)', () => {
        let shortString = 'love';
        let threeSpaces = '   ';
        expect(shortString.length).toBe(FILL_ME_IN);
        expect(threeSpaces.length).toBe(FILL_ME_IN);
    });

    it('should understand concatenating (joining) strings', () => {
        let simpleWord = '';
        simpleWord += 'L';
        expect(simpleWord).toEqual(FILL_ME_IN);
        simpleWord += 'O';
        expect(simpleWord).toEqual(FILL_ME_IN);
        simpleWord += 'V';
        expect(simpleWord).toEqual(FILL_ME_IN);
        simpleWord += 'E';
        expect(simpleWord).toEqual(FILL_ME_IN);

        simpleWord -= 'E';
        expect(typeof simpleWord).toEqual(FILL_ME_IN);

        let simplePhrase = 'i love';
        simplePhrase += ' you';
        expect(simplePhrase).toEqual(FILL_ME_IN);
    });

    it('should understand slicing strings', () => {
        let simpleText = 'I love you. Always and forever.';
        expect(simpleText.slice(0, 10)).toEqual(FILL_ME_IN);
        expect(simpleText.slice(12)).toEqual(FILL_ME_IN);
    });

    it('should understand string immutability', () => {
        let simpleWord = 'far';
        simpleWord[0] = 'c';
        expect(simpleWord).toEqual(FILL_ME_IN);
    });

    it('should create numbers', () => {
        let one = 1;
        let two = '2';
        expect(typeof one).toBe(FILL_ME_IN);
        expect(typeof two).toBe(FILL_ME_IN);
    });

    it('should understand number operations and symbols', () => {
        expect(3+2).toBe(FILL_ME_IN);
        expect(3-2).toBe(FILL_ME_IN);
        expect(3*2).toBe(FILL_ME_IN);
        expect(6/2).toBe(FILL_ME_IN);
        expect(3**2).toBe(FILL_ME_IN);
        expect(7%2).toBe(FILL_ME_IN);
    });

    it('should understand number coercion', () => {
        expect(2 == '2').toBe(FILL_ME_IN);
        expect(2 === '2').toBe(FILL_ME_IN);
    });
})
